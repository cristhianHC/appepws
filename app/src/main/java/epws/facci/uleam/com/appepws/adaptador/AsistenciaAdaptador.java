package epws.facci.uleam.com.appepws.adaptador;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.data.api.model.Asistencia;

/**
 * Created by cristhianhc on 01/08/17.
 */

public class AsistenciaAdaptador extends RecyclerView.Adapter<AsistenciaAdaptador.AsistenciaViewHolder> implements Filterable {
    private static Context context;
    private List<Asistencia> listaAsistencia;
    private static List<Asistencia> asistenciaFiltro = new ArrayList<>();

    private AsistenciaAdaptador.CustomFilter mFilter;

    public static class AsistenciaViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView txtEstado;
        public TextView txtFecha;
        public TextView txtObservacion;

        public AsistenciaViewHolder(View v) {
            super(v);
            txtEstado = (TextView) v.findViewById(R.id.txtEstado);
            txtFecha = (TextView) v.findViewById(R.id.txtFecha);
            txtObservacion = (TextView) v.findViewById(R.id.txtObservacion);
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AsistenciaActivity.crearInstancia(
                            (Activity) context, asistenciaFiltro.get(getAdapterPosition()));
                }
            });*/
        }
    }


    public AsistenciaAdaptador(Context context) {
        this.context = context;
        this.mFilter = new AsistenciaAdaptador.CustomFilter(AsistenciaAdaptador.this);
    }

    @Override
    public int getItemCount() {
        return asistenciaFiltro.size();
    }

    @Override
    public AsistenciaAdaptador.AsistenciaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_asistencia, viewGroup, false);
        return new AsistenciaAdaptador.AsistenciaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AsistenciaAdaptador.AsistenciaViewHolder viewHolder, final int i) {
        viewHolder.txtEstado.setText(asistenciaFiltro.get(i).getEstado());
        viewHolder.txtFecha.setText(asistenciaFiltro.get(i).getFecha());
        viewHolder.txtObservacion.setText(asistenciaFiltro.get(i).getObservacion());
    }

    public void swapCursor(List<Asistencia> lista) {
        listaAsistencia = lista;
        asistenciaFiltro.clear();
        asistenciaFiltro.addAll(listaAsistencia);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    /*Filtro*/
    public class CustomFilter extends Filter {
        private AsistenciaAdaptador AsistenciaAdaptador;

        private CustomFilter(AsistenciaAdaptador AsistenciaAdaptador) {
            super();
            this.AsistenciaAdaptador = AsistenciaAdaptador;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            asistenciaFiltro.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                asistenciaFiltro.addAll(listaAsistencia);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Asistencia promocion : listaAsistencia) {
                    if (promocion.getEstado().toLowerCase().contains(filterPattern)) {
                        asistenciaFiltro.add(promocion);
                    }
                }
            }
            results.values = asistenciaFiltro;
            results.count = asistenciaFiltro.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            this.AsistenciaAdaptador.notifyDataSetChanged();
        }
    }
}
