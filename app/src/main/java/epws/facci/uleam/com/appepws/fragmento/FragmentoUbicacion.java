package epws.facci.uleam.com.appepws.fragmento;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import epws.facci.uleam.com.appepws.FirstMapFragment;
import epws.facci.uleam.com.appepws.R;

public class FragmentoUbicacion extends Fragment implements OnMapReadyCallback {

    public FragmentoUbicacion() {
    }

    private static FragmentoUbicacion fragmentoUbicacion;
    public static FragmentoUbicacion getInstancia(){
        return (fragmentoUbicacion == null) ? new FragmentoUbicacion(): fragmentoUbicacion;
    }

    private FirstMapFragment mFirstMapFragment;
    private GoogleMap googleMap;
    private Spinner mMapTypeSelector;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmento_ubicacion, container, false);
        getActivity().findViewById(R.id.barra).setVisibility(View.GONE);
        setToolbar();
        mMapTypeSelector = (Spinner) v.findViewById(R.id.map_type_selector);
        mMapTypeSelector.setVisibility(View.VISIBLE);
        mMapTypeSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                googleMap.setMapType(mMapTypes[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mFirstMapFragment = FirstMapFragment.newInstance();
        getFragmentManager()
                .beginTransaction()
                .add(R.id.mapView, mFirstMapFragment)
                .commit();
        // Registrar escucha onMapReadyCallback
        mFirstMapFragment.getMapAsync(this);

        return v;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_main);
        toolbar.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        LatLng posicion = new LatLng(-0.9633334143105179, -80.71349465551377);

        googleMap.addMarker(new MarkerOptions()
                .position(posicion)
                .title("ESCUELA")
                .snippet("William Shakespeare"));
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);

        CameraPosition cameraPosition = CameraPosition.builder()
                .target(posicion)
                .zoom(15)
                .build();

        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("geo:"+marker.getPosition().latitude+","+marker.getPosition().longitude+"?z=15&q="+marker.getPosition().latitude+","+marker.getPosition().longitude+"(Ruta)"));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
                return false;
            }
        });
    }
    private int mMapTypes[] = {
            GoogleMap.MAP_TYPE_NORMAL,
            GoogleMap.MAP_TYPE_SATELLITE,
            GoogleMap.MAP_TYPE_HYBRID,
            GoogleMap.MAP_TYPE_TERRAIN
    };
}
