package epws.facci.uleam.com.appepws.data.api.model;

import java.sql.Timestamp;

/**
 * Created by cristhianhc on 27/07/17.
 */

public class Noticia {

    private String descripcion;

    private String fecha;

    private String imagen;

    private String resumen;

    private String titulo;


    public Noticia(String descripcion, String fecha, String imagen, String resumen, String titulo) {
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.imagen = imagen;
        this.resumen = resumen;
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return this.fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getImagen() {
        return this.imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getResumen() {
        return this.resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
