package epws.facci.uleam.com.appepws.adaptador;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.List;

import epws.facci.uleam.com.appepws.NoticiaActivity;
import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.Sistema;
import epws.facci.uleam.com.appepws.data.api.model.Noticia;

/**
 * Created by cristhianhc on 01/12/16.
 */

public class NoticiaAdaptador extends RecyclerView.Adapter<NoticiaAdaptador.NoticiaViewHolder> implements Filterable {
    private static Context context;
    private List<Noticia> promociones;
    private static List<Noticia> promocionesFiltro = new ArrayList<>();

    private NoticiaAdaptador.CustomFilter mFilter;

    public static class NoticiaViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView txtTitulo;
        public TextView txtFecha;
        public TextView txtResumen;
        public ImageView imagen;

        public NoticiaViewHolder(View v) {
            super(v);
            txtTitulo = (TextView) v.findViewById(R.id.txtTitulo);
            txtFecha = (TextView) v.findViewById(R.id.txtFecha);
            txtResumen = (TextView) v.findViewById(R.id.txtResumen);
            imagen = (ImageView) v.findViewById(R.id.imagen);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NoticiaActivity.crearInstancia(
                            (Activity) context, promocionesFiltro.get(getAdapterPosition()));
                }
            });
        }
    }


    public NoticiaAdaptador(Context context) {
        this.context = context;
        this.mFilter = new NoticiaAdaptador.CustomFilter(NoticiaAdaptador.this);
    }

    @Override
    public int getItemCount() {
        return promocionesFiltro.size();
    }

    @Override
    public NoticiaAdaptador.NoticiaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_noticia, viewGroup, false);
        return new NoticiaAdaptador.NoticiaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NoticiaAdaptador.NoticiaViewHolder viewHolder, final int i) {
        viewHolder.txtTitulo.setText(promocionesFiltro.get(i).getTitulo());
        viewHolder.txtFecha.setText(new Timestamp(new Long(promocionesFiltro.get(i).getFecha())).toString());
        viewHolder.txtResumen.setText(promocionesFiltro.get(i).getResumen());
        viewHolder.imagen.setImageBitmap(Sistema.decodificarImagen(promocionesFiltro.get(i).getImagen()));
    }

    public void swapCursor(List<Noticia> lista) {
        promociones = lista;
        promocionesFiltro.clear();
        promocionesFiltro.addAll(promociones);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    /*Filtro*/
    public class CustomFilter extends Filter {
        private NoticiaAdaptador NoticiaAdaptador;

        private CustomFilter(NoticiaAdaptador NoticiaAdaptador) {
            super();
            this.NoticiaAdaptador = NoticiaAdaptador;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            promocionesFiltro.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                promocionesFiltro.addAll(promociones);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Noticia promocion : promociones) {
                    if (promocion.getTitulo().toLowerCase().contains(filterPattern)) {
                        promocionesFiltro.add(promocion);
                    }
                }
            }
            results.values = promocionesFiltro;
            results.count = promocionesFiltro.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            this.NoticiaAdaptador.notifyDataSetChanged();
        }
    }
}
