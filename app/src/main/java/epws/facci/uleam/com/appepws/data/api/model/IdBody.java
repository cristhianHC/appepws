package epws.facci.uleam.com.appepws.data.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Objeto plano Java para representar el cuerpo de la petición POST /affiliates/login
 */
public class IdBody {
    @SerializedName("id")
    private Integer id;
    private Integer idSecundario;

    public IdBody(Integer id, Integer idSecundario) {
        this.id = id;
        this.idSecundario = idSecundario;
    }

    public IdBody(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdSecundario() {
        return idSecundario;
    }

    public void setIdSecundario(Integer idSecundario) {
        this.idSecundario = idSecundario;
    }
}
