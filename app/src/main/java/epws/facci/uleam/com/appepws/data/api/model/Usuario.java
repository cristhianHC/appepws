package epws.facci.uleam.com.appepws.data.api.model;

/**
 * Objeto plano Java para representar afiliados
 */
public class Usuario {

    private Integer id;

    private String apellido;

    private String cargo;

    private String cedula;

    private String email;

    private Boolean estado;

    private String funcion;

    private String nombre;

    private String password;

    private String username;

    private String token;


    public Usuario(Integer id,String apellido, String cargo, String cedula, String email, Boolean estado, String funcion, String nombre, String password, String username, String token) {
        this.id = id;
        this.apellido = apellido;
        this.cargo = cargo;
        this.cedula = cedula;
        this.email = email;
        this.estado = estado;
        this.funcion = funcion;
        this.nombre = nombre;
        this.password = password;
        this.username = username;
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getFuncion() {
        return funcion;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}