package epws.facci.uleam.com.appepws.fragmento;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;

import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.Sistema;
import epws.facci.uleam.com.appepws.adaptador.NoticiaAdaptador;
import epws.facci.uleam.com.appepws.data.api.RestApi;
import epws.facci.uleam.com.appepws.data.api.model.Noticia;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentoInicio extends Fragment implements SearchView.OnQueryTextListener {
    public FragmentoInicio() {
    }

    private static FragmentoInicio fragmentoInicio;

    public static FragmentoInicio getInstancia() {
        return (fragmentoInicio == null) ? new FragmentoInicio() : fragmentoInicio;
    }

    private RestApi mRestApi;

    private RecyclerView recyclerView;
    private NoticiaAdaptador noticiaAdaptador;

    private SwipeRefreshLayout refreshLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmento_inicio, container, false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        setToolbar();
        mRestApi = Sistema.getRestApi();

        recyclerView = (RecyclerView) v.findViewById(R.id.recicler_noticia);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        noticiaAdaptador = new NoticiaAdaptador(getActivity());
        recyclerView.setAdapter(noticiaAdaptador);

        refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefresh);
        // Iniciar la tarea asíncrona al revelar el indicador

        if (Sistema.isOnline(getActivity())) {
            getActivity().findViewById(R.id.barra).setVisibility(View.VISIBLE);
            new HackingBackgroundTask().execute();
        } else {
            Sistema.showLoginError(getActivity(), getString(R.string.error_red));
        }

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (Sistema.isOnline(getActivity())) {
                            //Sistema.showLoginError(getActivity(), "Sincronizando");
                            new HackingBackgroundTask().execute();
                        } else {
                            Sistema.showLoginError(getActivity(), getString(R.string.error_red));
                            refreshLayout.setRefreshing(false);
                        }
                    }
                }
        );
        return v;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_main);
        toolbar.setVisibility(View.VISIBLE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        noticiaAdaptador.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        noticiaAdaptador.getFilter().filter(newText);
        return false;
    }

    private class HackingBackgroundTask extends AsyncTask<Object, Object, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Object... params) {
            cargarAdaptador();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    private void cargarAdaptador() {

        Call<ArrayList<Noticia>> loginCall = mRestApi.getListaNoticia();
        loginCall.enqueue(new Callback<ArrayList<Noticia>>() {
            @Override
            public void onResponse(Call<ArrayList<Noticia>> call, Response<ArrayList<Noticia>> response) {
                if (!response.isSuccessful()) {
                    try {
                        String error = "Ha ocurrido un error. Contacte al administrador";
                        Sistema.showLoginError(getActivity(), error);
                        getActivity().findViewById(R.id.barra).setVisibility(View.GONE);
                        refreshLayout.setRefreshing(false);
                        Log.d("FragmentoInicio", response.errorBody().string());
                    } catch (IOException e) {
                            e.printStackTrace();
                    }
                    return;
                }
                try {
                    getActivity().findViewById(R.id.barra).setVisibility(View.GONE);
                }catch (Exception e){
                    e.printStackTrace();
                }
                ArrayList<Noticia> lista = response.body();
                noticiaAdaptador.swapCursor(lista);
                recyclerView.setAdapter(noticiaAdaptador);
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ArrayList<Noticia>> call, Throwable t) {

            }
        });
    }
}
