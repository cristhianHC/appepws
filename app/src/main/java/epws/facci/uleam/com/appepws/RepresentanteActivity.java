package epws.facci.uleam.com.appepws;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import epws.facci.uleam.com.appepws.data.prefs.SessionPrefs;
import epws.facci.uleam.com.appepws.fragmento.FragmentoAsistencia;
import epws.facci.uleam.com.appepws.fragmento.FragmentoParcial;
import epws.facci.uleam.com.appepws.fragmento.FragmentoQuimestre;

public class RepresentanteActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_representante);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_representante);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final FragmentoAsistencia fragmentoAsistencia = FragmentoAsistencia.getInstancia();
        final FragmentoParcial fragmentoParcial = FragmentoParcial.getInstancia();
        final FragmentoQuimestre fragmentoQuimestre = FragmentoQuimestre.getInstancia();


        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentContainer, fragmentoAsistencia).commit();
        }
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                switch (item.getItemId()) {
                    case (R.id.item_asistencia):
                        fragmentTransaction.replace(R.id.fragmentContainer, fragmentoAsistencia).commit();
                        break;
                    case (R.id.item_parcial):
                        fragmentTransaction.replace(R.id.fragmentContainer, fragmentoParcial).commit();
                        break;
                    case (R.id.item_quimestre):
                        fragmentTransaction.replace(R.id.fragmentContainer, fragmentoQuimestre).commit();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_main, menu);
        menu.findItem(R.id.salir).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.salir:
                SessionPrefs.get(this).logOut();
                startActivity(new Intent(this, MainActivity.class));
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
