package epws.facci.uleam.com.appepws;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import epws.facci.uleam.com.appepws.data.api.RestApi;
import epws.facci.uleam.com.appepws.data.api.model.ApiError;
import epws.facci.uleam.com.appepws.data.api.model.Asignatura;
import epws.facci.uleam.com.appepws.data.api.model.CalificacionQuimestre;
import epws.facci.uleam.com.appepws.data.api.model.IdBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cristhianhc on 06/08/17.
 */

public class QuimestreActivity extends AppCompatActivity {

    private TextView txtParcial1, txtParcial2, txtParcial3, txtPromedio,txtPromedio80, txtExamen,  txtExamen20, txtNota, txtObservacion;
    private CardView cardViewParcial1, cardViewParcial2, cardViewParcial3, cardViewPromedio80, cardViewExamen, cardViewPromedio, cardViewExamen20, cardViewNota,cardViewObservacion;

    public static void crearInstancia(Activity activity, Asignatura asignatura) {
        Intent intent = new Intent(activity, QuimestreActivity.class);
        intent.putExtra(Sistema.ASIGNATURA_ID, asignatura.getId());
        intent.putExtra(Sistema.ASIGNATURA_ID_ASIGNATURA, asignatura.getIdAsignatura());
        activity.startActivity(intent);

    }


    CalificacionQuimestre calificacionQuimestre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quimestre);
        Intent i = getIntent();
        IdBody idBody = new IdBody(i.getIntExtra(Sistema.ASIGNATURA_ID, 0), i.getIntExtra(Sistema.ASIGNATURA_ID_ASIGNATURA, 0));
        txtParcial1 = (TextView) findViewById(R.id.txt_parcial1);
        txtParcial2 = (TextView) findViewById(R.id.txt_parcial2);
        txtParcial3 = (TextView) findViewById(R.id.txt_parcial3);
        txtPromedio = (TextView) findViewById(R.id.txt_promedio);
        txtPromedio80 = (TextView) findViewById(R.id.txt_promedio80);
        txtExamen = (TextView) findViewById(R.id.txt_examen);
        txtExamen20 = (TextView) findViewById(R.id.txt_examen20);
        txtNota = (TextView) findViewById(R.id.txt_nota);
        txtObservacion = (TextView) findViewById(R.id.txt_observacion);

        cardViewParcial1 = (CardView) findViewById(R.id.cv_parcial1);
        cardViewParcial2 = (CardView) findViewById(R.id.cv_parcial2);
        cardViewParcial3 = (CardView) findViewById(R.id.cv_parcial3);
        cardViewPromedio80 = (CardView) findViewById(R.id.cv_promedio80);
        cardViewExamen = (CardView) findViewById(R.id.cv_examen);
        cardViewPromedio = (CardView) findViewById(R.id.cv_promedio);
        cardViewExamen20 = (CardView) findViewById(R.id.cv_examen20);
        cardViewNota = (CardView) findViewById(R.id.cv_nota);
        cardViewObservacion = (CardView) findViewById(R.id.cv_observacion);
        cargarAdaptador(idBody);
    }

    private void cargarAdaptador(IdBody idBody) {

        RestApi mRestApi = Sistema.getRestApi();

        Call<CalificacionQuimestre> loginCall;

        loginCall = mRestApi.getCalificacionQuimestre(idBody);

        loginCall.enqueue(new Callback<CalificacionQuimestre>() {
            @Override
            public void onResponse(Call<CalificacionQuimestre> call, Response<CalificacionQuimestre> response) {
                if (!response.isSuccessful()) {
                    String error = "Ha ocurrido un error. Contacte al administrador";
                    try {
                        // Reportar causas de error no relacionado con la API
                        Log.d("ParcialActivity", response.errorBody().string());
                        if (response.errorBody()
                                .contentType()
                                .subtype()
                                .equals("json")) {
                            ApiError apiError = ApiError.fromResponseBody(response.errorBody());

                            error = apiError.getMessage();
                            Log.d("ParcialActivity", apiError.getDeveloperMessage());
                        }

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                    return;
                }
                calificacionQuimestre = response.body();
                llenar();
            }

            @Override
            public void onFailure(Call<CalificacionQuimestre> call, Throwable t) {

            }
        });
    }

    private void llenar() {
        if (calificacionQuimestre.getParcial1() == null || calificacionQuimestre.getParcial1().equals("")) {
            cardViewParcial1.setVisibility(View.GONE);
        } else {
            txtParcial1.setText(calificacionQuimestre.getParcial1());
        }
        if (calificacionQuimestre.getParcial2() == null || calificacionQuimestre.getParcial2().equals("")) {
            cardViewParcial2.setVisibility(View.GONE);
        } else {
            txtParcial2.setText(calificacionQuimestre.getParcial2());
        }
        if (calificacionQuimestre.getParcial3() == null || calificacionQuimestre.getParcial3().equals("")) {
            cardViewParcial3.setVisibility(View.GONE);
        } else {
            txtParcial3.setText(calificacionQuimestre.getParcial3());
        }
        if (calificacionQuimestre.getPromedio80() == null || calificacionQuimestre.getPromedio80().equals("")) {
            cardViewPromedio80.setVisibility(View.GONE);
        } else {
            txtPromedio80.setText(calificacionQuimestre.getPromedio80());
        }
        if (calificacionQuimestre.getExamen() == null || calificacionQuimestre.getExamen().equals("")) {
            cardViewExamen.setVisibility(View.GONE);
        } else {
            txtExamen.setText(calificacionQuimestre.getExamen());
        }
        if (calificacionQuimestre.getPromedioParcial() == null || calificacionQuimestre.getPromedioParcial().equals("")) {
            cardViewPromedio.setVisibility(View.GONE);
        } else {
            txtPromedio.setText(calificacionQuimestre.getPromedioParcial());
        }
        if (calificacionQuimestre.getExamen20() == null || calificacionQuimestre.getExamen20().equals("")) {
            cardViewExamen20.setVisibility(View.GONE);
        } else {
            txtExamen20.setText(calificacionQuimestre.getExamen20());
        }
        if (calificacionQuimestre.getNota() == null || calificacionQuimestre.getNota().equals("")) {
            cardViewNota.setVisibility(View.GONE);
        } else {
            txtNota.setText(calificacionQuimestre.getNota());
        }
        if (calificacionQuimestre.getObservacion()==null || calificacionQuimestre.getObservacion().equals("")) {
            cardViewObservacion.setVisibility(View.GONE);
        } else {
            txtObservacion.setText(calificacionQuimestre.getObservacion());
        }
    }
}
