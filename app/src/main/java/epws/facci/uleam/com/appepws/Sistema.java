package epws.facci.uleam.com.appepws;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.widget.Toast;

import epws.facci.uleam.com.appepws.data.api.RestApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cristhianhc on 27/07/17.
 */

public class Sistema {

    public static String img = "";

    //Constante para enviar datos mediante putExtra

    public static final String NOTICIA_DESCRIPCION = "NOTICIA_DESCRIPCION";
    public static final String NOTICIA_RESUMEN = "NOTICIA_RESUMEN";
    public static final String NOTICIA_TITULO = "NOTICIA_TITULO";
    public static final String NOTICIA_FECHA = "NOTICIA_FECHA";

    public static final String PARCIAL_ID = "PARCIAL_ID";
    public static final String PARCIAL_DESCRIPCION = "PARCIAL_DESCRIPCION";
    public static final String QUIMESTRE_DESCRIPCION = "QUIMESTRE_DESCRIPCION";
    public static final String QUIMESTRE_ID = "QUIMESTRE_ID";

    public static final String ASIGNATURA_ID = "ASIGNATURA_ID";
    public static final String ASIGNATURA_ID_ASIGNATURA = "ASIGNATURA_ID_ASIGNATURA";


    private static Retrofit mRestAdapter;
    private static RestApi mRestApi;

    public static RestApi getRestApi(){
        if (mRestAdapter == null) {
            mRestAdapter = new Retrofit.Builder()
                    .baseUrl(RestApi.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        // Crear conexión a la API de SaludMock
        if (mRestApi == null){
            mRestApi = mRestAdapter.create(RestApi.class);
        }
        return mRestApi;
    }
    //Verificar conexion a internet
    public static boolean isOnline(Activity activity) {
        ConnectivityManager cm =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    //Mostrar mensajes
    public static void showLoginError(Activity activity,String error) {
        Toast.makeText(activity, error, Toast.LENGTH_LONG).show();
    }

    public static Bitmap decodificarImagen(String codigo) {
        Bitmap bitmap = null;
        try {
            byte[] byteData = Base64.decode(codigo, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(byteData, 0,
                    byteData.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
