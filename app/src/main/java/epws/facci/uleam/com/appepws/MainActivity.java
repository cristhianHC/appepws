package epws.facci.uleam.com.appepws;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import epws.facci.uleam.com.appepws.data.prefs.SessionPrefs;
import epws.facci.uleam.com.appepws.fragmento.FragmentoAcercaDe;
import epws.facci.uleam.com.appepws.fragmento.FragmentoInicio;
import epws.facci.uleam.com.appepws.fragmento.FragmentoLogin;
import epws.facci.uleam.com.appepws.fragmento.FragmentoUbicacion;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    SessionPrefs sessionPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);
        //Instancia de Fragmentos
        final FragmentoInicio fragmentoInicio = FragmentoInicio.getInstancia();
        final FragmentoUbicacion fragmentoUbicacion = FragmentoUbicacion.getInstancia();
        final FragmentoAcercaDe fragmentoAcercaDe = FragmentoAcercaDe.getInstancia();

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragmentContainer, fragmentoInicio).commit();
        }
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                switch (item.getItemId()) {
                    case (R.id.item_inicio):
                        fragmentTransaction.replace(R.id.fragmentContainer, fragmentoInicio).commit();
                        break;
                    case (R.id.item_ubicacion):
                        fragmentTransaction.replace(R.id.fragmentContainer, fragmentoUbicacion).commit();
                        break;
                    case (R.id.item_acerca_de):
                        fragmentTransaction.replace(R.id.fragmentContainer, fragmentoAcercaDe).commit();
                        break;
                }
                return true;
            }
        });
    }

    private void showAppointmentsScreen() {
        startActivity(new Intent(this, RepresentanteActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_main, menu);
        menu.findItem(R.id.entrar).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.entrar:
                sessionPrefs = SessionPrefs.get(MainActivity.this);
                if (SessionPrefs.isLoggedIn()) {
                    showAppointmentsScreen();
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainer, FragmentoLogin.getInstancia()).commit();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
