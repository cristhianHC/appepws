package epws.facci.uleam.com.appepws.fragmento;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;

import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.Sistema;
import epws.facci.uleam.com.appepws.adaptador.QuimestreAdaptador;
import epws.facci.uleam.com.appepws.data.api.RestApi;
import epws.facci.uleam.com.appepws.data.api.model.ApiError;
import epws.facci.uleam.com.appepws.data.api.model.Quimestre;
import epws.facci.uleam.com.appepws.data.api.model.IdBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentoQuimestre extends Fragment implements SearchView.OnQueryTextListener{

    public FragmentoQuimestre() {
    }

    private static FragmentoQuimestre fragmentoQuimestre;
    public static FragmentoQuimestre getInstancia(){
        return (fragmentoQuimestre == null) ? new FragmentoQuimestre(): fragmentoQuimestre;
    }

    private RestApi mRestApi;

    private RecyclerView recyclerView;
    private QuimestreAdaptador quimestreAdaptador;

    private SwipeRefreshLayout refreshLayout;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmento_quimestre, container, false);
        setToolbar();
        mRestApi = Sistema.getRestApi();

        recyclerView = (RecyclerView) v.findViewById(R.id.recicler_quimestre);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        quimestreAdaptador = new QuimestreAdaptador(getActivity());
        recyclerView.setAdapter(quimestreAdaptador);

        refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefresh);
        // Iniciar la tarea asíncrona al revelar el indicador

        if (Sistema.isOnline(getActivity())) {
           // Sistema.showLoginError(getActivity(),"Sincronizando");
            getActivity().findViewById(R.id.barra).setVisibility(View.VISIBLE);
            new FragmentoQuimestre.HackingBackgroundTask().execute();
            getActivity().findViewById(R.id.barra).setVisibility(View.GONE);
        } else {
            Sistema.showLoginError(getActivity(),getString(R.string.error_red));
        }

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (Sistema.isOnline(getActivity())) {
                          //  Sistema.showLoginError(getActivity(),"Sincronizando");
                            new FragmentoQuimestre.HackingBackgroundTask().execute();
                        } else {
                            Sistema.showLoginError(getActivity(),getString(R.string.error_red));
                            refreshLayout.setRefreshing(false);
                        }
                    }
                }
        );
        return v;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_representante);
        toolbar.setVisibility(View.VISIBLE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        quimestreAdaptador.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        quimestreAdaptador.getFilter().filter(newText);
        return false;
    }

    private class HackingBackgroundTask extends AsyncTask<Object, Object, Void> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Object... params) {
            cargarAdaptador();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    private void cargarAdaptador() {

        Call<ArrayList<Quimestre>> loginCall = mRestApi.getListaQuimestre();
        loginCall.enqueue(new Callback<ArrayList<Quimestre>>() {
            @Override
            public void onResponse(Call<ArrayList<Quimestre>> call, Response<ArrayList<Quimestre>> response) {
                if (!response.isSuccessful()) {
                    try {
                        String error = "Ha ocurrido un error. Contacte al administrador";
                        Sistema.showLoginError(getActivity(), error);
                        refreshLayout.setRefreshing(false);
                        // Reportar causas de error no relacionado con la API
                        Log.d("FragmentoQuimestre", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }
                ArrayList<Quimestre> lista = response.body();
                quimestreAdaptador.swapCursor(lista);
                recyclerView.setAdapter(quimestreAdaptador);
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ArrayList<Quimestre>> call, Throwable t) {

            }
        });
    }
}
