package epws.facci.uleam.com.appepws.fragmento;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import epws.facci.uleam.com.appepws.R;

public class FragmentoAcercaDe extends Fragment {
    public FragmentoAcercaDe() {
    }

    private static FragmentoAcercaDe fragmentoAcercaDe;
    public static FragmentoAcercaDe getInstancia(){
        return (fragmentoAcercaDe == null) ? new FragmentoAcercaDe(): fragmentoAcercaDe;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().findViewById(R.id.barra).setVisibility(View.GONE);
        setToolbar();
        return inflater.inflate(R.layout.fragmento_acerca_de, container, false);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_main);
        toolbar.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }
}
