package epws.facci.uleam.com.appepws.data.api.model;

/**
 * Created by cristhianhc on 06/08/17.
 */

public class CalificacionQuimestre {

    private Integer id;
    private String parcial1;
    private String parcial2;
    private String parcial3;
    private String promedioParcial;
    private String promedio80;
    private String examen;
    private String examen20;
    private String nota;
    private String observacion;

    public CalificacionQuimestre(Integer id, String parcial1, String parcial2, String parcial3, String promedioParcial, String promedio80, String examen, String examen20, String nota, String observacion) {
        this.id = id;
        this.parcial1 = parcial1;
        this.parcial2 = parcial2;
        this.parcial3 = parcial3;
        this.promedioParcial = promedioParcial;
        this.promedio80 = promedio80;
        this.examen = examen;
        this.examen20 = examen20;
        this.nota = nota;
        this.observacion = observacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParcial1() {
        return parcial1;
    }

    public void setParcial1(String parcial1) {
        this.parcial1 = parcial1;
    }

    public String getParcial2() {
        return parcial2;
    }

    public void setParcial2(String parcial2) {
        this.parcial2 = parcial2;
    }

    public String getParcial3() {
        return parcial3;
    }

    public void setParcial3(String parcial3) {
        this.parcial3 = parcial3;
    }

    public String getPromedioParcial() {
        return promedioParcial;
    }

    public void setPromedioParcial(String promedioParcial) {
        this.promedioParcial = promedioParcial;
    }

    public String getPromedio80() {
        return promedio80;
    }

    public void setPromedio80(String promedio80) {
        this.promedio80 = promedio80;
    }

    public String getExamen() {
        return examen;
    }

    public void setExamen(String examen) {
        this.examen = examen;
    }

    public String getExamen20() {
        return examen20;
    }

    public void setExamen20(String examen20) {
        this.examen20 = examen20;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
