package epws.facci.uleam.com.appepws.adaptador;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import epws.facci.uleam.com.appepws.AsignaturaActivity;
import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.data.api.model.Parcial;

/**
 * Created by cristhianhc on 03/08/17.
 */

public class ParcialAdaptador extends RecyclerView.Adapter<ParcialAdaptador.ParcialViewHolder> implements Filterable {
    private static Context context;
    private List<Parcial> listaParcial;
    private static List<Parcial> listaParcialFiltro = new ArrayList<>();

    private ParcialAdaptador.CustomFilter mFilter;

    public static class ParcialViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView txtDescripcion;

        public ParcialViewHolder(View v) {
            super(v);
            txtDescripcion = (TextView) v.findViewById(R.id.txtDescripcion);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AsignaturaActivity.crearInstancia(
                            (Activity) context, listaParcialFiltro.get(getAdapterPosition()));
                }
            });
        }
    }


    public ParcialAdaptador(Context context) {
        this.context = context;
        this.mFilter = new ParcialAdaptador.CustomFilter(ParcialAdaptador.this);
    }

    @Override
    public int getItemCount() {
        return listaParcialFiltro.size();
    }

    @Override
    public ParcialAdaptador.ParcialViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_parcial, viewGroup, false);
        return new ParcialAdaptador.ParcialViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ParcialAdaptador.ParcialViewHolder viewHolder, final int i) {
        viewHolder.txtDescripcion.setText(listaParcialFiltro.get(i).getDescripcion());
    }

    public void swapCursor(List<Parcial> lista) {
        listaParcial = lista;
        listaParcialFiltro.clear();
        listaParcialFiltro.addAll(listaParcial);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    /*Filtro*/
    public class CustomFilter extends Filter {
        private ParcialAdaptador ParcialAdaptador;

        private CustomFilter(ParcialAdaptador ParcialAdaptador) {
            super();
            this.ParcialAdaptador = ParcialAdaptador;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            listaParcialFiltro.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                listaParcialFiltro.addAll(listaParcial);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Parcial promocion : listaParcial) {
                    if (promocion.getDescripcion().toLowerCase().contains(filterPattern)) {
                        listaParcialFiltro.add(promocion);
                    }
                }
            }
            results.values = listaParcialFiltro;
            results.count = listaParcialFiltro.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            this.ParcialAdaptador.notifyDataSetChanged();
        }
    }
}
