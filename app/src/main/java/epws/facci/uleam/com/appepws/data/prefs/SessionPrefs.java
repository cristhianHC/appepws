package epws.facci.uleam.com.appepws.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import epws.facci.uleam.com.appepws.data.api.model.Usuario;

/**
 * Manejador de preferencias de la sesión del afiliado
 */
public class SessionPrefs {

    public static final String USUARIO = "USUARIO";
    public static final String USUARIO_ID = "USUARIO_ID";
    public static final String USUARIO_USERNAME = "USUARIO_USERNAME";
    public static final String USUARIO_NOMBRE = "USUARIO_NOMBRE";
    public static final String USUARIO_CEDULA = "USUARIO_CEDULA";
    public static final String USUARIO_FUNCION = "USUARIO_FUNCION";
    public static final String USUARIO_TOKEN = "USUARIO_TOKEN";

    private static SharedPreferences mPrefs;

    private static boolean mIsLoggedIn = false;

    private static SessionPrefs INSTANCE;

    public static SessionPrefs get(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SessionPrefs(context);
        }
        return INSTANCE;
    }

    private SessionPrefs(Context context) {
        mPrefs = context.getApplicationContext()
                .getSharedPreferences(USUARIO, Context.MODE_PRIVATE);

        mIsLoggedIn = !TextUtils.isEmpty(mPrefs.getString(USUARIO_USERNAME, null));
    }

    public static boolean isLoggedIn() {
        return mIsLoggedIn;
    }

    public static int getId() {
        return mPrefs.getInt(USUARIO_ID, 0);
    }

    public void saveAffiliate(Usuario usuario) {
        if (usuario != null) {
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putInt(USUARIO_ID, usuario.getId());
            editor.putString(USUARIO_USERNAME, usuario.getUsername());
            editor.putString(USUARIO_NOMBRE, usuario.getNombre());
            editor.putString(USUARIO_CEDULA, usuario.getCedula());
            editor.putString(USUARIO_FUNCION, usuario.getFuncion());
            editor.putString(USUARIO_TOKEN, usuario.getToken());
            editor.apply();

            mIsLoggedIn = true;
        }
    }

    public void logOut(){
        mIsLoggedIn = false;
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt(USUARIO_ID, 0);
        editor.putString(USUARIO_USERNAME, null);
        editor.putString(USUARIO_NOMBRE, null);
        editor.putString(USUARIO_CEDULA, null);
        editor.putString(USUARIO_FUNCION, null);
        editor.putString(USUARIO_TOKEN, null);
        editor.apply();
    }
}
