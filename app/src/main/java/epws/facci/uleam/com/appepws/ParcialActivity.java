package epws.facci.uleam.com.appepws;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import epws.facci.uleam.com.appepws.data.api.RestApi;
import epws.facci.uleam.com.appepws.data.api.model.ApiError;
import epws.facci.uleam.com.appepws.data.api.model.Asignatura;
import epws.facci.uleam.com.appepws.data.api.model.CalificacionParcial;
import epws.facci.uleam.com.appepws.data.api.model.IdBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cristhianhc on 03/08/17.
 */

public class ParcialActivity extends AppCompatActivity {

    private TextView txtTai, txtAic, txtAgc, txtL, txtEsumativa, txtPromedio, txtCualitativa, txtObservacion;
    private CardView cardViewTai, cardViewAic, cardViewAgc, cardViewL, cardViewEsumativa, cardViewPromedio, cardViewCualitativa, cardViewObservacion;

    public static void crearInstancia(Activity activity, Asignatura asignatura) {
        Intent intent = new Intent(activity, ParcialActivity.class);
        intent.putExtra(Sistema.ASIGNATURA_ID, asignatura.getId());
        intent.putExtra(Sistema.ASIGNATURA_ID_ASIGNATURA, asignatura.getIdAsignatura());
        activity.startActivity(intent);

    }


    CalificacionParcial calificacionParcial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcial);
        Intent i = getIntent();
        IdBody idBody = new IdBody(i.getIntExtra(Sistema.ASIGNATURA_ID, 0), i.getIntExtra(Sistema.ASIGNATURA_ID_ASIGNATURA, 0));
        txtTai = (TextView) findViewById(R.id.txt_tai);
        txtAic = (TextView) findViewById(R.id.txt_aic);
        txtAgc = (TextView) findViewById(R.id.txt_agc);
        txtL = (TextView) findViewById(R.id.txt_l);
        txtEsumativa = (TextView) findViewById(R.id.txt_esumativa);
        txtPromedio = (TextView) findViewById(R.id.txt_promedio);
        txtCualitativa = (TextView) findViewById(R.id.txt_cualitativa);
        txtObservacion = (TextView) findViewById(R.id.txt_observacion);

        cardViewTai = (CardView) findViewById(R.id.cv_tai);
        cardViewAic = (CardView) findViewById(R.id.cv_aic);
        cardViewAgc = (CardView) findViewById(R.id.cv_agc);
        cardViewL = (CardView) findViewById(R.id.cv_l);
        cardViewEsumativa = (CardView) findViewById(R.id.cv_esumativa);
        cardViewPromedio = (CardView) findViewById(R.id.cv_promedio);
        cardViewCualitativa = (CardView) findViewById(R.id.cv_cualitativa);
        cardViewObservacion = (CardView) findViewById(R.id.cv_observacion);
        cargarAdaptador(idBody);
    }

    private void cargarAdaptador(IdBody idBody) {

        RestApi mRestApi = Sistema.getRestApi();

        Call<CalificacionParcial> loginCall;

        loginCall = mRestApi.getCalificacionParcial(idBody);

        loginCall.enqueue(new Callback<CalificacionParcial>() {
            @Override
            public void onResponse(Call<CalificacionParcial> call, Response<CalificacionParcial> response) {
                if (!response.isSuccessful()) {
                    String error = "Ha ocurrido un error. Contacte al administrador";
                    try {
                        // Reportar causas de error no relacionado con la API
                        Log.d("ParcialActivity", response.errorBody().string());
                        if (response.errorBody()
                                .contentType()
                                .subtype()
                                .equals("json")) {
                            ApiError apiError = ApiError.fromResponseBody(response.errorBody());

                            error = apiError.getMessage();
                            Log.d("ParcialActivity", apiError.getDeveloperMessage());
                        }

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                    return;
                }
                calificacionParcial = response.body();
                llenar();
            }

            @Override
            public void onFailure(Call<CalificacionParcial> call, Throwable t) {

            }
        });
    }

    private void llenar() {
        if (calificacionParcial.getTai() == null || calificacionParcial.getTai().equals("")) {
            cardViewTai.setVisibility(View.GONE);
        } else {
            txtTai.setText(calificacionParcial.getTai());
        }
        if (calificacionParcial.getAgc() == null || calificacionParcial.getAgc().equals("")) {
            cardViewAgc.setVisibility(View.GONE);
        } else {
            txtAgc.setText(calificacionParcial.getAgc());
        }
        if (calificacionParcial.getAic() == null || calificacionParcial.getAic().equals("")) {
            cardViewAic.setVisibility(View.GONE);
        } else {
            txtAic.setText(calificacionParcial.getAic());
        }
        if (calificacionParcial.getL() == null || calificacionParcial.getL().equals("")) {
            cardViewL.setVisibility(View.GONE);
        } else {
            txtL.setText(calificacionParcial.getL());
        }
        if (calificacionParcial.getEsumativa() == null || calificacionParcial.getEsumativa().equals("")) {
            cardViewEsumativa.setVisibility(View.GONE);
        } else {
            txtEsumativa.setText(calificacionParcial.getEsumativa());
        }
        if (calificacionParcial.getPromedio() == null || calificacionParcial.getPromedio().equals("")) {
            cardViewPromedio.setVisibility(View.GONE);
        } else {
            txtPromedio.setText(calificacionParcial.getPromedio());
        }
        if (calificacionParcial.getCualitativa() == null || calificacionParcial.getCualitativa().equals("")) {
            cardViewCualitativa.setVisibility(View.GONE);
        } else {
            txtCualitativa.setText(calificacionParcial.getCualitativa());
        }
        if (calificacionParcial.getObservacion()==null || calificacionParcial.getObservacion().equals("")) {
            cardViewObservacion.setVisibility(View.GONE);
        } else {
            txtObservacion.setText(calificacionParcial.getObservacion());
        }
    }

}