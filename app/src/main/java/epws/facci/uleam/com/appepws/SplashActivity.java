package epws.facci.uleam.com.appepws;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import java.lang.ref.WeakReference;

public class SplashActivity extends AppCompatActivity {

    private final static int SPLASH_SCREEN_MIN_LENGTH = 1000;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mHandler.postDelayed(new waitTime(this), SPLASH_SCREEN_MIN_LENGTH);
    }

    private static class waitTime implements Runnable {

        private WeakReference mActivity;

        private waitTime(Activity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void run() {
            if (mActivity.get() != null) {
                Activity activity = (Activity) mActivity.get();
                activity.startActivity(new Intent(activity, MainActivity.class));
                activity.finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        mHandler = null;
    }

}