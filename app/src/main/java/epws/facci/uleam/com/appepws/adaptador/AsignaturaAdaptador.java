package epws.facci.uleam.com.appepws.adaptador;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;

import java.util.ArrayList;
import java.util.List;

import epws.facci.uleam.com.appepws.ParcialActivity;
import epws.facci.uleam.com.appepws.QuimestreActivity;
import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.Sistema;
import epws.facci.uleam.com.appepws.data.api.model.Asignatura;

/**
 * Created by cristhianhc on 03/08/17.
 */

public class AsignaturaAdaptador extends RecyclerView.Adapter<AsignaturaAdaptador.AsignaturaViewHolder> implements Filterable {
    private static Context context;
    private List<Asignatura> listaAsignatura;
    private static List<Asignatura> listaAsignaturaFiltro = new ArrayList<>();

    private AsignaturaAdaptador.CustomFilter mFilter;

    public static class AsignaturaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Campos respectivos de un item
        public TextView txtDescripcion;
        public TextView txtCurso;
        public TextView txtDocente;
        private FABToolbarLayout morph;

        private boolean whatsappInstalledOrNot(String uri) {
            PackageManager pm = ((Activity) context).getPackageManager();
            boolean app_installed = false;
            try {
                pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
                app_installed = true;
            } catch (PackageManager.NameNotFoundException e) {
                app_installed = false;
            }
            return app_installed;
        }

        public AsignaturaViewHolder(View v) {
            super(v);
            txtDescripcion = (TextView) v.findViewById(R.id.txtDescripcion);
            txtCurso = (TextView) v.findViewById(R.id.txtCurso);
            txtDocente = (TextView) v.findViewById(R.id.txtDocente);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = ((Activity) context).getIntent();
                    if (i.getIntExtra(Sistema.PARCIAL_ID,0)!=0){
                        ParcialActivity.crearInstancia((Activity)context,listaAsignaturaFiltro.get(getAdapterPosition()));
                    }
                    if (i.getIntExtra(Sistema.QUIMESTRE_ID,0)!=0){
                        QuimestreActivity.crearInstancia((Activity)context,listaAsignaturaFiltro.get(getAdapterPosition()));
                    }
                }
            });

            FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
            morph = (FABToolbarLayout) v.findViewById(R.id.fabtoolbar);

            View uno, dos, tres, cuatro;

            uno = v.findViewById(R.id.uno);
            dos = v.findViewById(R.id.dos);
            tres = v.findViewById(R.id.tres);
            cuatro = v.findViewById(R.id.cuatro);

            fab.setOnClickListener(this);
            uno.setOnClickListener(this);
            dos.setOnClickListener(this);
            tres.setOnClickListener(this);
            cuatro.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.fab:
                    morph.show();
                    break;

                case R.id.uno:
                    Uri num = Uri.parse("tel:" + listaAsignaturaFiltro.get(getAdapterPosition()).getCelular());
                    Intent i = new Intent(Intent.ACTION_CALL, num);
                    Activity ac = (Activity) context;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ActivityCompat.checkSelfPermission(ac, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(ac, new String[]{Manifest.permission.CALL_PHONE}, 105);
                        } else {
                            ac.startActivity(i);
                        }
                    } else {
                        ac.startActivity(i);
                    }

                    break;
                case R.id.dos:
                    boolean isWhatsappInstalled = whatsappInstalledOrNot("com.whatsapp");
                    if (isWhatsappInstalled) {
                        String whatsapp = listaAsignaturaFiltro.get(getAdapterPosition()).getCelular();
                        if (whatsapp != null) {
                            if (whatsapp.length() > 0) {
                                Intent sendIntent = new Intent("android.intent.action.MAIN");
                                sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("593" + whatsapp.substring(1, whatsapp.length())) + "@s.whatsapp.net");//phone number without "+" prefix
                                Activity activity = (Activity) context;
                                activity.startActivity(sendIntent);
                            } else {
                                //Config.showSnackBar((Activity) context, R.id.coordinator, "No existe numero registrado");
                            }
                        } else {
                            //Config.showSnackBar((Activity) context, R.id.coordinator, "No existe numero registrado");
                        }
                    } else {
                        //Config.showSnackBar((Activity) context, R.id.coordinator, "Whastapp no esta instalado");
                    }
                    break;
                case R.id.tres:
                    String[] TO = {listaAsignaturaFiltro.get(getAdapterPosition()).getCorreo()}; //Direcciones email  a enviar.
                    String[] CC = {""}; //Direcciones email con copia.

                    Intent emailIntent = new Intent(Intent.ACTION_SEND);

                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                    //emailIntent.putExtra(Intent.EXTRA_CC, CC);
//                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Tu Asunto...");
//                emailIntent.putExtra(Intent.EXTRA_TEXT, "tuemail@email.com"); // * configurar email aquí!

                    try {
                        Activity activity = (Activity) context;
                        activity.startActivity(Intent.createChooser(emailIntent, "Enviar email."));
                        Log.i("EMAIL", "Enviando email...");
                    }
                    catch (android.content.ActivityNotFoundException e) {
                        //Config.showSnackBar(getActivity(),R.id.coordinator,"NO existe ningún cliente de email instalado!.");
                    }
                    break;
                case R.id.cuatro:
                    morph.hide();
                    break;
                default:
                    break;
            }
        }
    }


    public AsignaturaAdaptador(Context context) {
        this.context = context;
        this.mFilter = new AsignaturaAdaptador.CustomFilter(AsignaturaAdaptador.this);
    }

    @Override
    public int getItemCount() {
        return listaAsignaturaFiltro.size();
    }

    @Override
    public AsignaturaAdaptador.AsignaturaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_asignatura, viewGroup, false);
        return new AsignaturaAdaptador.AsignaturaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AsignaturaAdaptador.AsignaturaViewHolder viewHolder, final int i) {
        viewHolder.txtDescripcion.setText(listaAsignaturaFiltro.get(i).getDescripcion());
        viewHolder.txtCurso.setText(listaAsignaturaFiltro.get(i).getCurso());
        viewHolder.txtDocente.setText(listaAsignaturaFiltro.get(i).getNota());
    }

    public void swapCursor(List<Asignatura> lista) {
        listaAsignatura = lista;
        listaAsignaturaFiltro.clear();
        listaAsignaturaFiltro.addAll(listaAsignatura);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    /*Filtro*/
    public class CustomFilter extends Filter {
        private AsignaturaAdaptador AsignaturaAdaptador;

        private CustomFilter(AsignaturaAdaptador AsignaturaAdaptador) {
            super();
            this.AsignaturaAdaptador = AsignaturaAdaptador;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            listaAsignaturaFiltro.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                listaAsignaturaFiltro.addAll(listaAsignatura);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Asignatura promocion : listaAsignatura) {
                    if (promocion.getDescripcion().toLowerCase().contains(filterPattern)) {
                        listaAsignaturaFiltro.add(promocion);
                    }
                }
            }
            results.values = listaAsignaturaFiltro;
            results.count = listaAsignaturaFiltro.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            this.AsignaturaAdaptador.notifyDataSetChanged();
        }
    }
}
