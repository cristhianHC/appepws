package epws.facci.uleam.com.appepws.data.api.model;

/**
 * Created by cristhianhc on 03/08/17.
 */

public class Parcial {


    private int id;
    private String descripcion;

    public Parcial(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
