package epws.facci.uleam.com.appepws.adaptador;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import epws.facci.uleam.com.appepws.AsignaturaActivity;
import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.data.api.model.Quimestre;

/**
 * Created by cristhianhc on 03/08/17.
 */

public class QuimestreAdaptador extends RecyclerView.Adapter<QuimestreAdaptador.QuimestreViewHolder> implements Filterable {
    private static Context context;
    private List<Quimestre> listaQuimestre;
    private static List<Quimestre> listaQuimestreFiltro = new ArrayList<>();

    private QuimestreAdaptador.CustomFilter mFilter;

    public static class QuimestreViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView txtDescripcion;

        public QuimestreViewHolder(View v) {
            super(v);
            txtDescripcion = (TextView) v.findViewById(R.id.txtDescripcion);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AsignaturaActivity.crearInstancia(
                            (Activity) context, listaQuimestreFiltro.get(getAdapterPosition()));
                }
            });
        }
    }


    public QuimestreAdaptador(Context context) {
        this.context = context;
        this.mFilter = new QuimestreAdaptador.CustomFilter(QuimestreAdaptador.this);
    }

    @Override
    public int getItemCount() {
        return listaQuimestreFiltro.size();
    }

    @Override
    public QuimestreAdaptador.QuimestreViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.lista_quimestre, viewGroup, false);
        return new QuimestreAdaptador.QuimestreViewHolder(v);
    }

    @Override
    public void onBindViewHolder(QuimestreAdaptador.QuimestreViewHolder viewHolder, final int i) {
        viewHolder.txtDescripcion.setText(listaQuimestreFiltro.get(i).getDescripcion());
    }

    public void swapCursor(List<Quimestre> lista) {
        listaQuimestre = lista;
        listaQuimestreFiltro.clear();
        listaQuimestreFiltro.addAll(listaQuimestre);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    /*Filtro*/
    public class CustomFilter extends Filter {
        private QuimestreAdaptador QuimestreAdaptador;

        private CustomFilter(QuimestreAdaptador QuimestreAdaptador) {
            super();
            this.QuimestreAdaptador = QuimestreAdaptador;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            listaQuimestreFiltro.clear();
            final FilterResults results = new FilterResults();
            if (constraint.length() == 0) {
                listaQuimestreFiltro.addAll(listaQuimestre);
            } else {
                final String filterPattern = constraint.toString().toLowerCase().trim();
                for (final Quimestre promocion : listaQuimestre) {
                    if (promocion.getDescripcion().toLowerCase().contains(filterPattern)) {
                        listaQuimestreFiltro.add(promocion);
                    }
                }
            }
            results.values = listaQuimestreFiltro;
            results.count = listaQuimestreFiltro.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            this.QuimestreAdaptador.notifyDataSetChanged();
        }
    }
}
