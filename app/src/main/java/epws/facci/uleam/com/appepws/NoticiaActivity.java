package epws.facci.uleam.com.appepws;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import epws.facci.uleam.com.appepws.data.api.model.Noticia;

public class NoticiaActivity extends AppCompatActivity {

    public static void crearInstancia(Activity activity, Noticia noticia) {
        Intent intent = new Intent(activity, NoticiaActivity.class);
        intent.putExtra(Sistema.NOTICIA_DESCRIPCION, noticia.getDescripcion());
        intent.putExtra(Sistema.NOTICIA_RESUMEN, noticia.getResumen());
        intent.putExtra(Sistema.NOTICIA_FECHA, noticia.getFecha());
        intent.putExtra(Sistema.NOTICIA_TITULO, noticia.getTitulo());
        Sistema.img = noticia.getImagen();
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noticia);
        setToolbar();// Añadir action bar
        if (getSupportActionBar() != null) // Habilitar up button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView txtDescripcion = (TextView) findViewById(R.id.txtObservacion);
        ImageView imagen = (ImageView) findViewById(R.id.image_paralax);

        Intent i = getIntent();
        imagen.setImageBitmap(Sistema.decodificarImagen(Sistema.img));
        CollapsingToolbarLayout collapser =
                (CollapsingToolbarLayout) findViewById(R.id.collapser);
        collapser.setTitle(i.getStringExtra(Sistema.NOTICIA_TITULO)); // Cambiar título
        txtDescripcion.setText(i.getStringExtra(Sistema.NOTICIA_DESCRIPCION));
    }

    private void setToolbar() {
        // Añadir la Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_noticia);
        setSupportActionBar(toolbar);
    }

}
