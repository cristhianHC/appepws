package epws.facci.uleam.com.appepws.data.api.model;

/**
 * Created by cristhianhc on 03/08/17.
 */

public class Asignatura {

    private int id;
    private int idAsignatura;
    private String descripcion;
    private String curso;
    private String docente;
    private String nota;
    private String celular;
    private String correo;

    public Asignatura(int id, int idAsignatura, String descripcion, String curso, String docente, String nota, String celular, String correo) {
        this.id = id;
        this.idAsignatura = idAsignatura;
        this.descripcion = descripcion;
        this.curso = curso;
        this.docente = docente;
        this.nota = nota;
        this.celular = celular;
        this.correo = correo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAsignatura() {
        return idAsignatura;
    }

    public void setIdAsignatura(int idAsignatura) {
        this.idAsignatura = idAsignatura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getDocente() {
        return docente;
    }

    public void setDocente(String docente) {
        this.docente = docente;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
