package epws.facci.uleam.com.appepws.fragmento;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import epws.facci.uleam.com.appepws.AsignaturaActivity;
import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.Sistema;
import epws.facci.uleam.com.appepws.data.api.model.Asignatura;
import epws.facci.uleam.com.appepws.data.api.model.Parcial;
import epws.facci.uleam.com.appepws.data.api.model.Quimestre;

/**
 * Created by cristhianhc on 03/08/17.
 */

public class FragmentoNotaParcial extends Fragment {

    private TextView txtTai, txtAic, txtAgc, txtL, txtEsumativa, txtPromedio, txtCualitativa, txtObservacion;
    private GridLayout gridTai, gridAic, gridAgc, gridL, gridEsumativa, gridPromedio, gridCualitativa, gridObservacion;

    public static void crearInstancia(Activity activity, Asignatura asignatura) {
        Intent intent = new Intent(activity, AsignaturaActivity.class);
        intent.putExtra(Sistema.ASIGNATURA_ID, asignatura.getId());
        intent.putExtra(Sistema.ASIGNATURA_ID_ASIGNATURA, asignatura.getIdAsignatura());
        activity.startActivity(intent);

    }

    public FragmentoNotaParcial() {
    }

    static FragmentoNotaParcial fragmentoNotaParcial;

    public static FragmentoNotaParcial getInstancia() {
        return (fragmentoNotaParcial == null) ? new FragmentoNotaParcial() : fragmentoNotaParcial;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmento_nota_parcial, container, false);
        txtTai = (TextView) v.findViewById(R.id.txt_tai);
        txtAic = (TextView) v.findViewById(R.id.txt_aic);
        txtAgc = (TextView) v.findViewById(R.id.txt_agc);
        txtL = (TextView) v.findViewById(R.id.txt_l);
        txtEsumativa = (TextView) v.findViewById(R.id.txt_esumativa);
        txtPromedio = (TextView) v.findViewById(R.id.txt_promedio);
        txtCualitativa = (TextView) v.findViewById(R.id.txt_cualitativa);
        txtObservacion = (TextView) v.findViewById(R.id.txt_observacion);

        gridTai = (GridLayout) v.findViewById(R.id.grid_tai);
        gridAic = (GridLayout) v.findViewById(R.id.grid_aic);
        gridAgc = (GridLayout) v.findViewById(R.id.grid_agc);
        gridL = (GridLayout) v.findViewById(R.id.grid_l);
        gridEsumativa = (GridLayout) v.findViewById(R.id.grid_esumativa);
        gridPromedio = (GridLayout) v.findViewById(R.id.grid_promedio);
        gridCualitativa = (GridLayout) v.findViewById(R.id.grid_cualitativa);
        gridObservacion = (GridLayout) v.findViewById(R.id.grid_observacion);
        return v;
    }
}
