package epws.facci.uleam.com.appepws.fragmento;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;

import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.Sistema;
import epws.facci.uleam.com.appepws.adaptador.AsistenciaAdaptador;
import epws.facci.uleam.com.appepws.data.api.RestApi;
import epws.facci.uleam.com.appepws.data.api.model.Asistencia;
import epws.facci.uleam.com.appepws.data.api.model.IdBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static epws.facci.uleam.com.appepws.data.prefs.SessionPrefs.USUARIO;
import static epws.facci.uleam.com.appepws.data.prefs.SessionPrefs.USUARIO_ID;

public class FragmentoAsistencia extends Fragment implements SearchView.OnQueryTextListener{

    public FragmentoAsistencia() {
    }

    static FragmentoAsistencia fragmentoAsistencia;
    public static FragmentoAsistencia getInstancia(){
        return (fragmentoAsistencia == null) ? new FragmentoAsistencia(): fragmentoAsistencia;
    }

    private RestApi mRestApi;

    private RecyclerView recyclerView;
    private AsistenciaAdaptador asistenciaAdaptador;
    private SwipeRefreshLayout refreshLayout;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmento_asistencia, container, false);
        setToolbar();
        mRestApi = Sistema.getRestApi();

        recyclerView = (RecyclerView) v.findViewById(R.id.recicler_asistencia);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        asistenciaAdaptador = new AsistenciaAdaptador(getActivity());
        recyclerView.setAdapter(asistenciaAdaptador);

        refreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefresh);
        // Iniciar la tarea asíncrona al revelar el indicador

        if (Sistema.isOnline(getActivity())) {
            //Sistema.showLoginError(getActivity(),"Sincronizando");
            getActivity().findViewById(R.id.barra).setVisibility(View.VISIBLE);
            new HackingBackgroundTask().execute();
            getActivity().findViewById(R.id.barra).setVisibility(View.GONE);
        } else {
            Sistema.showLoginError(getActivity(),getString(R.string.error_red));
        }

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (Sistema.isOnline(getActivity())) {
                            //Sistema.showLoginError(getActivity(),"Sincronizando");
                            new HackingBackgroundTask().execute();
                        } else {
                            Sistema.showLoginError(getActivity(),getString(R.string.error_red));
                            refreshLayout.setRefreshing(false);
                        }
                    }
                }
        );
        return v;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_representante);
        toolbar.setVisibility(View.VISIBLE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        asistenciaAdaptador.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        asistenciaAdaptador.getFilter().filter(newText);
        return false;
    }

    private class HackingBackgroundTask extends AsyncTask<Object, Object, Void> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Object... params) {
            cargarAdaptador();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    private void cargarAdaptador() {
        int a = getContext().getApplicationContext().getSharedPreferences(USUARIO, Context.MODE_PRIVATE).getInt(USUARIO_ID,0);
        Call<ArrayList<Asistencia>> loginCall = mRestApi.getListaAsistencia(new IdBody(a));
        loginCall.enqueue(new Callback<ArrayList<Asistencia>>() {
            @Override
            public void onResponse(Call<ArrayList<Asistencia>> call, Response<ArrayList<Asistencia>> response) {
                if (!response.isSuccessful()) {
                    try {
                        String error = "Ha ocurrido un error. Contacte al administrador";
                        Sistema.showLoginError(getActivity(), error);
                        refreshLayout.setRefreshing(false);
                        // Reportar causas de error no relacionado con la API
                        Log.d("FragmentoAsistencia", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }
                ArrayList<Asistencia> lista = response.body();
                asistenciaAdaptador.swapCursor(lista);
                recyclerView.setAdapter(asistenciaAdaptador);
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ArrayList<Asistencia>> call, Throwable t) {

            }
        });
    }
}
