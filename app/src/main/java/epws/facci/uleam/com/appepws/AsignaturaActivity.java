package epws.facci.uleam.com.appepws;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;
import java.util.ArrayList;

import epws.facci.uleam.com.appepws.adaptador.AsignaturaAdaptador;
import epws.facci.uleam.com.appepws.data.api.RestApi;
import epws.facci.uleam.com.appepws.data.api.model.ApiError;
import epws.facci.uleam.com.appepws.data.api.model.Asignatura;
import epws.facci.uleam.com.appepws.data.api.model.IdBody;
import epws.facci.uleam.com.appepws.data.api.model.Parcial;
import epws.facci.uleam.com.appepws.data.api.model.Quimestre;
import epws.facci.uleam.com.appepws.data.prefs.SessionPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static epws.facci.uleam.com.appepws.data.prefs.SessionPrefs.USUARIO;
import static epws.facci.uleam.com.appepws.data.prefs.SessionPrefs.USUARIO_ID;

public class AsignaturaActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    public static void crearInstancia(Activity activity, Object objeto) {
        if (objeto instanceof Parcial){
        Intent intent = new Intent(activity, AsignaturaActivity.class);
        intent.putExtra(Sistema.PARCIAL_ID, ((Parcial) objeto).getId());
        intent.putExtra(Sistema.PARCIAL_DESCRIPCION, ((Parcial) objeto).getDescripcion());
            intent.putExtra(Sistema.QUIMESTRE_ID, 0);
            intent.putExtra(Sistema.QUIMESTRE_DESCRIPCION, "");
        activity.startActivity(intent);
        }
        else if (objeto instanceof Quimestre){
        Intent intent = new Intent(activity, AsignaturaActivity.class);
        intent.putExtra(Sistema.QUIMESTRE_ID, ((Quimestre) objeto).getId());
        intent.putExtra(Sistema.QUIMESTRE_DESCRIPCION, ((Quimestre) objeto).getDescripcion());
            intent.putExtra(Sistema.PARCIAL_ID, 0);
            intent.putExtra(Sistema.PARCIAL_DESCRIPCION, "");
        activity.startActivity(intent);
        }
    }

    private RestApi mRestApi;

    private RecyclerView recyclerView;
    private AsignaturaAdaptador asignaturaAdaptador;

    private SwipeRefreshLayout refreshLayout;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asignatura);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_asignatura);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Intent i = getIntent();
        getSupportActionBar().setTitle(i.getStringExtra(Sistema.PARCIAL_DESCRIPCION)+i.getStringExtra(Sistema.QUIMESTRE_DESCRIPCION));

        mRestApi = Sistema.getRestApi();

        recyclerView = (RecyclerView) findViewById(R.id.recicler_asignatura);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        asignaturaAdaptador = new AsignaturaAdaptador(this);
        recyclerView.setAdapter(asignaturaAdaptador);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);

        if (Sistema.isOnline(this)) {
            Sistema.showLoginError(this,"Sincronizando");
            new HackingBackgroundTask().execute();
        } else {
            Sistema.showLoginError(this,getString(R.string.error_red));
        }

        refreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        if (Sistema.isOnline(AsignaturaActivity.this)) {
                            Sistema.showLoginError(AsignaturaActivity.this,"Sincronizando");
                            new HackingBackgroundTask().execute();
                        } else {
                            Sistema.showLoginError(AsignaturaActivity.this,getString(R.string.error_red));
                        }
                    }
                }
        );
    }

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(this, RepresentanteActivity.class));
        finish();;
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_main, menu);
        menu.findItem(R.id.salir).setVisible(true);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.salir:
                SessionPrefs.get(this).logOut();
                startActivity(new Intent(this, MainActivity.class));
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        asignaturaAdaptador.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        asignaturaAdaptador.getFilter().filter(newText);
        return false;
    }

    private void cargarAdaptador() {

        Call<ArrayList<Asignatura>> loginCall = null;

        Intent i = getIntent();
        int a = getSharedPreferences(USUARIO, Context.MODE_PRIVATE).getInt(USUARIO_ID,0);
        if (i.getIntExtra(Sistema.PARCIAL_ID,0)!=0){
            loginCall = mRestApi.getListaAsignaturaParcial(new IdBody(a,i.getIntExtra(Sistema.PARCIAL_ID,0)));
        }
        if (i.getIntExtra(Sistema.QUIMESTRE_ID,0)!=0){
            loginCall = mRestApi.getListaAsignaturaQuimestre(new IdBody(a,i.getIntExtra(Sistema.QUIMESTRE_ID,0)));
        }

        loginCall.enqueue(new Callback<ArrayList<Asignatura>>() {
            @Override
            public void onResponse(Call<ArrayList<Asignatura>> call, Response<ArrayList<Asignatura>> response) {
                if (!response.isSuccessful()) {
                    String error = "Ha ocurrido un error. Contacte al administrador";
                    try {
                        // Reportar causas de error no relacionado con la API
                        Log.d("FragmentoLogin", response.errorBody().string());
                        if (response.errorBody()
                                .contentType()
                                .subtype()
                                .equals("json")) {
                            ApiError apiError = ApiError.fromResponseBody(response.errorBody());

                            error = apiError.getMessage();
                            Log.d("FragmentoLogin", apiError.getDeveloperMessage());
                        }

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                    findViewById(R.id.barra).setVisibility(View.GONE);
                    refreshLayout.setRefreshing(false);
                    return;
                }
                ArrayList<Asignatura> lista = response.body();
                asignaturaAdaptador.swapCursor(lista);
                recyclerView.setAdapter(asignaturaAdaptador);
                findViewById(R.id.barra).setVisibility(View.GONE);
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ArrayList<Asignatura>> call, Throwable t) {

            }
        });
    }

    private class HackingBackgroundTask extends AsyncTask<Object, Object, Void> {

        @Override
        protected void onPreExecute() {
            findViewById(R.id.barra).setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Object... params) {
            cargarAdaptador();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }
}
