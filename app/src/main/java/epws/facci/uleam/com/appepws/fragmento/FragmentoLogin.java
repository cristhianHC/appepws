package epws.facci.uleam.com.appepws.fragmento;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

import epws.facci.uleam.com.appepws.R;
import epws.facci.uleam.com.appepws.RepresentanteActivity;
import epws.facci.uleam.com.appepws.Sistema;
import epws.facci.uleam.com.appepws.data.api.RestApi;
import epws.facci.uleam.com.appepws.data.api.model.ApiError;
import epws.facci.uleam.com.appepws.data.api.model.LoginBody;
import epws.facci.uleam.com.appepws.data.api.model.Usuario;

import epws.facci.uleam.com.appepws.data.prefs.SessionPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentoLogin extends Fragment {

    public FragmentoLogin() {
        // Required empty public constructor
    }

    static FragmentoLogin fragmentoLogin;
    public static FragmentoLogin getInstancia(){
        return (fragmentoLogin == null) ? new FragmentoLogin():fragmentoLogin;
    }

    private RestApi mRestApi;

    // UI references.
    private ImageView logo;
    private EditText usuario;
    private EditText password;
    private TextInputLayout floatUsuario;
    private TextInputLayout floatPassword;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmento_login, container, false);
        setToolbar();

        mRestApi = Sistema.getRestApi();

        Button btLogin = (Button) v.findViewById(R.id.bt_login);
        logo = (ImageView) v.findViewById(R.id.image_logo);
        usuario = (EditText) v.findViewById(R.id.usuario);
        password = (EditText) v.findViewById(R.id.password);
        floatUsuario = (TextInputLayout) v.findViewById(R.id.float_usuario);
        floatPassword = (TextInputLayout) v.findViewById(R.id.float_password);
        mLoginFormView = v.findViewById(R.id.login_form);
        mProgressView = v.findViewById(R.id.login_progress);

        // Setup
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    if (!Sistema.isOnline(getActivity())) {
                        Sistema.showLoginError(getActivity(),getString(R.string.error_red));
                        return true;
                    }
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Sistema.isOnline(getActivity())) {
                    Sistema.showLoginError(getActivity(),getString(R.string.error_red));
                    return;
                }
                attemptLogin();

            }
        });
        return v;
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar_main);
        toolbar.setVisibility(View.GONE);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }

    private void attemptLogin() {

        // Reset errors.
        floatUsuario.setError(null);
        floatPassword.setError(null);

        // Store values at the time of the login attempt.
        String userId = usuario.getText().toString();
        String passwordId = password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(passwordId)) {
            floatPassword.setError(getString(R.string.error_campo_requerido));
            focusView = floatPassword;
            cancel = true;
        }

        // Verificar si el ID tiene contenido.
        if (TextUtils.isEmpty(userId)) {
            floatUsuario.setError(getString(R.string.error_campo_requerido));
            focusView = floatUsuario;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Mostrar el indicador de carga y luego iniciar la petición asíncrona.
            showProgress(true);

            Call<Usuario> loginCall = mRestApi.login(new LoginBody(userId, passwordId));
            loginCall.enqueue(new Callback<Usuario>() {
                @Override
                public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                    // Mostrar progreso
                    showProgress(false);

                    // Procesar errores
                    if (!response.isSuccessful()) {
                        String error = "Ha ocurrido un error. Contacte al administrador";
                        if (response.errorBody()
                                .contentType()
                                .subtype()
                                .equals("json")) {
                            ApiError apiError = ApiError.fromResponseBody(response.errorBody());

                            error = apiError.getMessage();
                            Log.d("FragmentoLogin", apiError.getDeveloperMessage());
                        } else {
                            try {
                                // Reportar causas de error no relacionado con la API
                                Log.d("FragmentoLogin", response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        Sistema.showLoginError(getActivity(),error);
                        return;
                    }
                    if (response.body() != null){
                    // Guardar afiliado en preferencias
                    SessionPrefs.get(getContext()).saveAffiliate(response.body());

                    // Ir a la citas médicas
                    showAppointmentsScreen();}
                    else{
                        Sistema.showLoginError(getActivity(),"Imposible acceder");
                    }
                }

                @Override
                public void onFailure(Call<Usuario> call, Throwable t) {
                    showProgress(false);
                    Sistema.showLoginError(getActivity(),t.getMessage());
                }
            });
        }
    }

    private void showProgress(boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);

        int visibility = show ? View.GONE : View.VISIBLE;
        logo.setVisibility(visibility);
        mLoginFormView.setVisibility(visibility);
    }

    private void showAppointmentsScreen() {
        startActivity(new Intent(getActivity(), RepresentanteActivity.class));
        getActivity().finish();
    }
    
}
