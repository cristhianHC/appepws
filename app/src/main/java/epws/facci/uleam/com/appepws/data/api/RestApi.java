package epws.facci.uleam.com.appepws.data.api;

import java.util.ArrayList;

import epws.facci.uleam.com.appepws.data.api.model.Asignatura;
import epws.facci.uleam.com.appepws.data.api.model.CalificacionParcial;
import epws.facci.uleam.com.appepws.data.api.model.CalificacionQuimestre;
import epws.facci.uleam.com.appepws.data.api.model.IdBody;
import epws.facci.uleam.com.appepws.data.api.model.Asistencia;
import epws.facci.uleam.com.appepws.data.api.model.LoginBody;
import epws.facci.uleam.com.appepws.data.api.model.Noticia;
import epws.facci.uleam.com.appepws.data.api.model.Parcial;
import epws.facci.uleam.com.appepws.data.api.model.Quimestre;
import epws.facci.uleam.com.appepws.data.api.model.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * REST service de SaludMock
 */

public interface RestApi {

    // TODO: Cambiar host por "10.0.3.2" para Genymotion.
    // TODO: Cambiar host por "10.0.2.2" para AVD.
    // TODO: Cambiar host por IP de tu PC para dispositivo real.
    //public static final String BASE_URL = "http://192.168.1.3:8080/epws/rest/";
    //public static final String BASE_URL = "http://epws.eastus.cloudapp.azure.com:8080/epws/rest/";
    public static final String BASE_URL = "http://13.82.183.216/rest/";
    @POST("todo/login")
    Call<Usuario> login(@Body LoginBody loginBody);

    @POST("todo/asistencia")
    Call<ArrayList<Asistencia>> getListaAsistencia(@Body IdBody idBody);

    @POST("todo/asignatura_parcial")
    Call<ArrayList<Asignatura>> getListaAsignaturaParcial(@Body IdBody idBody);

    @POST("todo/asignatura_quimestre")
    Call<ArrayList<Asignatura>> getListaAsignaturaQuimestre(@Body IdBody idBody);

    @POST("todo/nota_parcial")
    Call<CalificacionParcial> getCalificacionParcial(@Body IdBody idBody);

    @POST("todo/nota_quimestre")
    Call<CalificacionQuimestre> getCalificacionQuimestre(@Body IdBody idBody);

    @GET("todo/noticia")
    Call<ArrayList<Noticia>> getListaNoticia();

    @GET("todo/parcial")
    Call<ArrayList<Parcial>> getListaParcial();

    @GET("todo/quimestre")
    Call<ArrayList<Quimestre>> getListaQuimestre();
}
